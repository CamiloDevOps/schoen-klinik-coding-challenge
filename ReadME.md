# Schön Klinik Code Challenge:

### Goal: create your own DevOps playground to run and monitor an application on Docker basis:
<br>

### 1. Setup a K8s cluster by script on your notebook or anywhere (script language, operating system etc. of your choice)

<br>

### Solution:

Give executions permissions to `cluster.sh` exclusively to the current user with: 
<br>

```sh 
chmod u+x ./cluster.sh
```
<br>

To install Minikube and its dependencies, as well as set it up for observability with the Kubernetes dashboard run:
<br>

```sh 
./cluster.sh install
```

<br>

Or remove after done with:
```sh 
./cluster.sh uninstall
``` 

<br>

--- 
<br>

### 2. Setup and configure a CI/CD pipeline of your choice (Github Actions, Gitlab CI/CD or anything similar etc.)

<br>

### Solution:

For this task I decided to install a Gitlab runner in the cluster which can be used to run pipelines on it and at the same time use it as test app to be monitored in the 3rd task. 

<br>
To deploy the Gitlab Runner to the cluster give executions permissions to `./gitlab.sh` exclusively to the current user with: 

<br> 

```sh 
chmod u+x ./gitlab.sh
```
<br>

and install with

```sh 
./gitlab.sh install
```

<br>

Uninstall thereafter with:

```sh
./gitlab.sh uninstall
```

<br>

The Gitlab pipeline configure in the file `.gitlab-ci.yml` runs 4 different jobs:

- `install_monitoring`: A job which can be triggered manually to install Prometheus and Grafana.
The file `kubernetes/gitlab-runner-privileges.yml` details the permissions needed for the Gitlab runner to make changes on the cluster.

- `test`: Runs the unit test of a second test application called petclinic
- `build`: Builds the Java application petclinic with Maven
- `deploy`: Builds petclinic and pushes a container to the Public Docker registry. It also respawns the container in the Kubernetes cluster and sets up monitoring dashboards.

<br>

--- 

<br> 

### 3. Deploy an application of your choice, deploy Prometheus and Grafana or any similar monitoring stack on that K8s cluster using the CI/CD pipeline. Monitor some metrics of the application.

### Solution:

I decided to deploy a Gitlab runner on my cluster to be able to run the pipeline jobs on my own and monitor is load during deployment of pipeline jobs for a demo app called petclinic. `Petclinic` is a Java based app using the Spring Framework. It provides Unit test and easy packaging with Maven. The Kubernetes deployment ensure there is always two replicas running it.

The Prometheus Kube stack is deployed by triggering a manual job which either install the application if it is not already installed or updates the deployment with Helm. The `Dashboards` for monitoring the applications are added in the deploy job where they can be created in the petclinic namespace.


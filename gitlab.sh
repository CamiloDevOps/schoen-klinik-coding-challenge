#!/bin/bash

if [ $1 == "install" ]; then
    
    # check if minikube is running
    eval $(minikube docker-env)

    # Check and create gitlab namespace
    kubectl create namespace gitlab --dry-run=client -o yaml | kubectl apply -f -
    
    echo "Installing Gitlab Runner"
    helm upgrade --install --namespace gitlab gitlab-runner -f helm/values-gitlab-runner.yml gitlab/gitlab-runner  --create-namespace
    #kubectl apply -f kubernetes/gitlab-runner-privileges.yml
fi

if [ $1 == "uninstall" ]; then

    echo "Uninstalling Gitlab chart"
    helm uninstall gitlab-runner -n gitlab
    kubectl delete namespace gitlab
fi


## *** Useful commands ***
#  If needed use this commands to inspect the deployment in detail:

# attach to a gitlab-runner container 
# kubectl exec -it gitlab-runner-78984967f4-p9gd4 -n gitlab  -- /bin/bash

# ** Gitlab runner CLI commands **
# install runner service
# gitlab-runner install --user root --working-directory /home/gitlab-runner/

# Runner Registration of runners with the Gitlab API
# Note: Since the gitlab-runner CLI has its own register function marked as deprecated,
# cURL can be used instead for automation purposes.
# curl --request POST "https://gitlab.com/api/v4/runners" --form "token=GR1348941SXm29Yj_i8kVGGicPe6q" --form "description=test-1-20150125-test" --form "tag_list=build,test"
#!/bin/bash


if [ $1 == "install" ]; then
    # Update Ubuntus package lists and upgrade existing packages
    echo "Updating OS";
    sudo apt-get update

    # Install Docker
    echo "Installing Docker";
    #sudo apt-get install -y docker.io

    # Install minikube
    echo "Installing Minikube";
    curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
    chmod +x minikube
    mv minikube /usr/local/bin/
    minikube version
   
    # Install kubectl
    echo "Installing Kubectl";
    curl -LO "https://dl.k8s.io/release/`curl -L https://dl.k8s.io/release/stable.txt`/bin/linux/amd64/kubectl"
    chmod +x kubectl
    sudo mv kubectl /usr/local/bin/

    # Start minikube
    # The settings for cpus and memory may need to be changed depending on the machine running the minikube cluster     
    echo "Creating cluster";
    minikube start --cpus 3 --memory 5072
    minikube addons enable metrics-server
    minikube status
   
    echo "Starting Kubernetes dashboard"
    minikube dashboard &

    # Install helm
    echo "Installing Helm 3";
    curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
    chmod 700 get_helm.sh
    ./get_helm.sh
fi

if [ $1 == "uninstall" ]; then
    echo "Uninstalling minikube";
    minikube stop && minikube delete --purge=true
fi

if [ $1 == "extra" ]; then
    echo "Installing NGINX Ingress controller";

    # install NGINX Custom resources using repository
    git clone https://github.com/nginxinc/kubernetes-ingress.git --branch v3.0.1 && \
    cd kubernetes-ingress/deployments/helm-chart && \
    helm repo add nginx-stable https://helm.nginx.com/stable || \
    helm repo update ||
    helm install nginx-ing-controller . 

    # set up SSL for domain
    echo "Setting up SSL/TLS Certificate";

    sudo apt install snapd
    sudo snap install core; sudo snap refresh core
    sudo snap install --classic certbot
    sudo ln -s /snap/bin/certbot /usr/bin/certbot
    sudo certbot --nginx
    sudo certbot certonly --standalone -d camilofernandez.de
fi

